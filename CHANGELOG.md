# indymilter changelog

## 0.2.0 (2023-01-09)

### Added

* The new methods `Command::into_message` and `Reply::parse_reply` have been
  added to complement the existing command and reply methods. They complete the
  low-level API and are intended to support uses outside of indymilter itself.
* Implementations of `From<SocketAddr>` and `From<CString>` have been added for
  `SocketInfo`.
* Some additional remarks on usage have been added to the API documentation.

### Changed

* The `body` callback now receives the message body chunks as `bytes::Bytes`
  instead of as `Vec<u8>`, avoiding an additional copy of the body contents.
  Milters using the `body` callback may need to add a direct dependency on the
  [bytes] crate.
* `Stage` has been renamed to `MacroStage`. Its `all_stages*` methods have been
  renamed to `all*`.
* `ProtoOpts::HEADER_LEADING_SPACE` has been renamed to
  `ProtoOpts::LEADING_SPACE`.

### Removed

* The unused enum variant `ParseCommandError::UnsupportedProtocolVersion` has
  been removed.

[bytes]: https://crates.io/crates/bytes

## 0.1.1 (2022-05-04)

### Changed

* The default value of `Config::connection_timeout` has been increased from 305
  to 7210 seconds. 7210 seconds, or slightly above two hours, is the timeout
  duration used in libmilter.
* During option negotiation, milter protocol versions below version 6 are now
  accepted. While using such ancient versions is not advisable, they are now
  supported just like in libmilter.

## 0.1.0 (2022-02-11)

Initial release.
