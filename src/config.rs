// indymilter – asynchronous milter library
// Copyright © 2021–2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use crate::proto_util::Actions;
use std::time::Duration;

/// Milter configuration.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Config {
    /// The maximum number of connections open at any time.
    ///
    /// The default is 100.
    pub max_connections: usize,

    /// Actions to enable during the `eom` stage.
    ///
    /// This setting is overridden by the actions requested through
    /// [`NegotiateContext::requested_actions`][crate::context::NegotiateContext::requested_actions]
    /// in the `negotiate` stage.
    ///
    /// The default is empty.
    pub actions: Actions,

    /// The connection timeout.
    ///
    /// This timeout concerns reading/writing of entire milter protocol messages
    /// to the connection. This limit would be reached eg when a milter client
    /// idles and does not send the next command. When the timeout expires the
    /// client’s connection is closed.
    ///
    /// See also Postfix’s `milter_*_timeout` parameters.
    ///
    /// The default is 7210 seconds.
    pub connection_timeout: Duration,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            max_connections: 100,
            actions: Default::default(),
            // The default timeout duration is slightly above two hours. This
            // value is the same as in libmilter, whose documentation advises
            // not to decrease it.
            connection_timeout: Duration::from_secs(7210),
        }
    }
}
