// indymilter – asynchronous milter library
// Copyright © 2021–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use std::io;
use tokio::net::TcpListener;
#[cfg(unix)]
use tokio::net::UnixListener;

/// A Tokio-based listener.
#[derive(Debug)]
pub enum Listener {
    /// A TCP socket listener.
    Tcp(TcpListener),
    /// A UNIX domain socket listener.
    #[cfg(unix)]
    Unix(UnixListener),
}

/// Conversion to a listener.
pub trait IntoListener {
    /// Converts this value to a listener.
    fn into_listener(self) -> io::Result<Listener>;
}

impl IntoListener for std::net::TcpListener {
    fn into_listener(self) -> io::Result<Listener> {
        TcpListener::from_std(self).map(Listener::Tcp)
    }
}

#[cfg(unix)]
impl IntoListener for std::os::unix::net::UnixListener {
    fn into_listener(self) -> io::Result<Listener> {
        UnixListener::from_std(self).map(Listener::Unix)
    }
}

impl IntoListener for TcpListener {
    fn into_listener(self) -> io::Result<Listener> {
        Ok(Listener::Tcp(self))
    }
}

#[cfg(unix)]
impl IntoListener for UnixListener {
    fn into_listener(self) -> io::Result<Listener> {
        Ok(Listener::Unix(self))
    }
}

impl IntoListener for Listener {
    fn into_listener(self) -> io::Result<Listener> {
        Ok(self)
    }
}
