use indymilter::{Callbacks, Context, SocketInfo, Status};
use tokio::{net::TcpListener, signal};

#[tokio::main]
async fn main() {
    let listener = TcpListener::bind("127.0.0.1:9876")
        .await.expect("cannot open milter socket");

    let callbacks = Callbacks::new()
        .on_connect(|context, _, socket_info| {
            Box::pin(handle_connect(context, socket_info))
        });

    let config = Default::default();

    indymilter::run(listener, callbacks, config, signal::ctrl_c())
        .await
        .expect("milter execution failed");
}

async fn handle_connect(
    _: &mut Context<()>,
    socket_info: SocketInfo,
) -> Status {
    if let SocketInfo::Inet(addr) = socket_info {
        println!("connect from {}", addr.ip());
    }

    Status::Continue
}
