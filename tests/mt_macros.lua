local conn = mt.connect("inet:" .. port .. "@127.0.0.1")
assert(conn, "could not open connection")

local SMFIC_EOH = string.byte("N")
local err = mt.macro(conn, SMFIC_EOH, "{name}", "1234")
assert(err == nil, err)

local err = mt.eoh(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local SMFIC_EOM = string.byte("E")
local err = mt.macro(conn, SMFIC_EOM, "{name}", "5678")
assert(err == nil, err)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.disconnect(conn)
assert(err == nil, err)
