local conn = mt.connect("inet:" .. port .. "@127.0.0.1")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "mail.example.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

assert(mt.eom_check(conn, MT_HDRADD, "name1", "value1"));
assert(mt.eom_check(conn, MT_HDRADD, "name2", "value2"));

local err = mt.disconnect(conn)
assert(err == nil, err)
